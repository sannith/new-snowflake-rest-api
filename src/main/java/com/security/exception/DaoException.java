package com.security.exception;

public class DaoException extends RuntimeException {
    private static final long serialVersionUID = -1933874950323499560L;

    public DaoException() {
        super();
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(Exception e) {
        super(e);
    }

    public DaoException(String message, Exception e) {
        super(message, e);
    }
}
