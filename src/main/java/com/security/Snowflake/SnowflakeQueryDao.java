    package com.security.Snowflake;

    import com.security.exception.DaoException;

    import java.sql.Connection;
    import java.util.List;
    import java.util.Map;
    import java.util.concurrent.CompletableFuture;

    public interface SnowflakeQueryDao {
        public Map<String, String> getColumnMetaData(String viewName) throws DaoException;
        public CompletableFuture<List<Object>> getResultsSnowflake(String query, Connection connection, List<String> queryIds) throws DaoException;
    }
