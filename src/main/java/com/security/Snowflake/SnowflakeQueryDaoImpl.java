package com.security.Snowflake;

import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import javax.sql.DataSource;

import com.security.User_Api_Services.JwtAuthenticationFilter;
import com.security.User_Api_Services.UserService;
import com.security.exception.DaoException;
import net.snowflake.client.jdbc.SnowflakeResultSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.env.Environment;

@Component
@Scope("prototype")
public class SnowflakeQueryDaoImpl implements SnowflakeQueryDao {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private Environment env;
    static  ApplicationContext applicationContext;

    @Async("asyncExecutor")
    public CompletableFuture<List<Object>> getResultsSnowflake(String query, Connection connection,List<String> queryIds) throws DaoException {
        List<Object> resultList = new ArrayList<Object>();
        ResultSet resultSet = null;
        String queryID = "";
        try {
            resultSet = connection.createStatement().executeQuery(query);
            queryID = resultSet.unwrap(SnowflakeResultSet.class).getQueryID();
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();
            List<String> columnNames = new ArrayList<String>(columnCount);
            Map<String, Integer> columnTypes = new HashMap<String, Integer>(columnCount);
            for (int counter = 1; counter <= columnCount; counter++) {
                columnNames.add(resultSetMetaData.getColumnLabel(counter));
                columnTypes.put(resultSetMetaData.getColumnLabel(counter), resultSetMetaData.getColumnType(counter));
            }
            Map<String, Object> result = null;
            while (resultSet.next()) {
                result = new HashMap<String, Object>(columnCount);

                for (String columnName : columnNames) {
                    result.put(columnName, resultSet.getString(columnName));
                }
                resultList.add(result);
            }
            resultSet.close();
        } catch (Exception e) {
            throw new DaoException(e);
        }
        queryIds.add(queryID);
        return CompletableFuture.completedFuture(resultList);
    }

    public Map<String, String> getColumnMetaData(String viewName) throws DaoException {
        String query = "show columns in " + viewName;
        ResultSet resultSet = null;
        Connection connection = null;
        Map<String, String> columnTypes = null;
        UserService userService = new UserService();
        try {
            connection = userService.getSnowflakeCredentils(JwtAuthenticationFilter.getRefreshToken()).getConnection();//jdbcTemplate.getDataSource().getConnection();
            logger.debug(query);
            resultSet = connection.createStatement().executeQuery(query);
            columnTypes = new HashMap<String, String>();
            while (resultSet.next()) {
                String columnName = resultSet.getString("column_name");
                String dataType = getDataType(resultSet.getString("data_type"));
                String column_type = null;
                switch (dataType) {
                    case "TEXT":
                        column_type = "text";
                        break;
                    case "FIXED","REAL":
                        column_type = "number";
                        break;
                    case "TIMESTAMP_NTZ","TIMESTAMP_LTZ","TIMESTAMP_TZ","TIMESTAMP":
                        column_type = "timestamp";
                        break;
                    /*case "REAL":
                        column_type = "number";
                        break;*/
                    /*case "TIMESTAMP_LTZ":
                        column_type = "timestamp";
                        break;
                    case "TIMESTAMP_TZ":
                        column_type = "timestamp";
                        break;
                    case "TIMESTAMP":
                        column_type = "timestamp";
                        break;*/
                    case "VARIANT":
                        column_type = "variant";
                        break;
                    case "BOOLEAN":
                        column_type = "boolean";
                        break;
                    case "ARRAY":
                        column_type = "array";
                        break;
                    case "OBJECT":
                        column_type = "object";
                        break;
                    default:
                        column_type = "text";
                        break;
                }
                columnTypes.put(columnName.toLowerCase(), column_type);
            }
            resultSet.close();
            // connection.close();
            // return columnTypes;
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    // resultSet.close();
                }
                if (connection != null && !connection.isClosed()) {
                    // connection.close();
                }
            } catch (SQLException e1) {
                logger.error("Exception while closing connection", e1);
            }
        }
        System.out.println("------>  "+columnTypes);
        return columnTypes;

    }
    @SuppressWarnings("unchecked")
    private String getDataType(String dataType) {
        ObjectMapper mapper = new ObjectMapper();
        try { return ((Map<String,String>)mapper.readValue(dataType, Map.class)).get("type"); }
        catch (JsonParseException|JsonMappingException e) { e.printStackTrace(); }
        catch (IOException e) { e.printStackTrace(); }
        return null;
    }

}
