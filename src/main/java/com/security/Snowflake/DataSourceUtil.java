/*



package com.security.Snowflake;

import com.elysium.springboot.model.RequestToken;
import com.elysium.springboot.util.PassowrdDecryptionUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import io.micrometer.common.util.StringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Component
public class DataSourceUtil {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    PassowrdDecryptionUtil passowrdDecryptionUtil;
    Map<Map,DriverManagerDataSource> mapDataSourceMap=new HashMap<>();
    @Autowired
    private Environment env;

    @Autowired
    HttpServletRequest httpServletRequest;

    public DataSource snowflakeDataSource(RequestToken requestToken) {
        DriverManagerDataSource dataSource=null;
        if(requestToken == null  || requestToken.getDetails() != null || StringUtils.isEmpty(requestToken.getToken()) ||
                StringUtils.isEmpty(requestToken.getUserName()) || env.getProperty("app.apicall.switch").contains("off") ){
            logger.info("Using default DataSource as token is null");
            dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName("com.snowflake.client.jdbc.SnowflakeDriver");
            if(requestToken.getDetails() != null){
                logger.info("Using Details");
                String token = requestToken.getDetails();
                String s = new String(Base64.decodeBase64(token));
                JsonObject jsonObject = new JsonParser().parse(s).getAsJsonObject();
                String org_id = String.valueOf(requestToken.getOrgId());
                byte[] ivBytes = Base64.decodeBase64(jsonObject.get("iv").getAsString());
                byte[] acc = Base64.decodeBase64(jsonObject.get("data").getAsString());
                String accDetails = passowrdDecryptionUtil.decryptInput(org_id,ivBytes,acc);
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(accDetails);
                dataSource.setUrl("jdbc:snowflake://"+jsonElement.getAsJsonObject().get("account").getAsString()+".snowflakecomputing.com"); //ik55883.east-us-2.azure
                dataSource.setUsername(jsonElement.getAsJsonObject().get("user").getAsString());
                dataSource.setPassword(jsonElement.getAsJsonObject().get("password").getAsString());
                Properties connectionProperties = new Properties();
                connectionProperties.put("url", "jdbc:snowflake://"+jsonElement.getAsJsonObject().get("account").getAsString()+".snowflakecomputing.com");
                connectionProperties.put("user", jsonElement.getAsJsonObject().get("user").getAsString());
                connectionProperties.put("password", jsonElement.getAsJsonObject().get("password").getAsString());
                connectionProperties.put("account", jsonElement.getAsJsonObject().get("account").getAsString());
                connectionProperties.put("db", jsonElement.getAsJsonObject().get("database").getAsString());
                connectionProperties.put("schema", jsonElement.getAsJsonObject().get("schema").getAsString());
                connectionProperties.put("application", "ElysiumAnalytics");
                connectionProperties.put("queryTimeout",  String.valueOf(Integer.parseInt(requestToken.getSearchOptions().get("queryTimeOut"))*60));
                connectionProperties.put("warehouse", jsonElement.getAsJsonObject().get("warehouse").getAsString());
                dataSource.setConnectionProperties(connectionProperties);
            }else {
                dataSource.setUrl(env.getProperty("app.datasource.url")); //ik55883.east-us-2.azure
                dataSource.setUsername(env.getProperty("app.datasource.user"));
                dataSource.setPassword(env.getProperty("app.datasource.password"));
                Properties connectionProperties = new Properties();
                connectionProperties.put("url", env.getProperty("app.datasource.url"));
                connectionProperties.put("user", env.getProperty("app.datasource.user"));
                connectionProperties.put("password", env.getProperty("app.datasource.password"));
                connectionProperties.put("account", env.getProperty("app.datasource.account"));
                connectionProperties.put("db", env.getProperty("app.datasource.db"));
                connectionProperties.put("schema", env.getProperty("app.datasource.schema"));
                connectionProperties.put("application", "ElysiumAnalytics");
                connectionProperties.put("warehouse", env.getProperty("app.datasource.warehouse"));
                connectionProperties.put("queryTimeout",  String.valueOf(Integer.parseInt(requestToken.getSearchOptions().get("queryTimeOut"))*60));
                // connectionProperties.put("insecureMode", env.getProperty("app.datasource.insecuremode"));
                dataSource.setConnectionProperties(connectionProperties);
            }
            return dataSource;

        }else {
            Map<String, String> dbMap = getDataSourceMap(requestToken);
            dataSource = isDataSourceExist(dbMap);
            if (dataSource == null) {
                logger.info("===================Using new DataSource=================");
                dataSource = new DriverManagerDataSource();
                dataSource.setDriverClassName("com.snowflake.client.jdbc.SnowflakeDriver");
                dataSource.setUrl(dbMap.get("url")); //ik55883.east-us-2.azure
                dataSource.setUsername(dbMap.get("user"));
                dataSource.setPassword(dbMap.get("password"));
                Properties connectionProperties = new Properties();
                connectionProperties.put("url", dbMap.get("url"));
                connectionProperties.put("user", dbMap.get("user"));
                connectionProperties.put("password", dbMap.get("password"));
                connectionProperties.put("account", dbMap.get("account"));
                connectionProperties.put("db", dbMap.get("database"));
                connectionProperties.put("schema", dbMap.get("schema"));
                connectionProperties.put("application", dbMap.get("application"));
                connectionProperties.put("queryTimeout", dbMap.get("queryTimeout"));

//                connectionProperties.put("role", dbMap.get("role"));
                connectionProperties.put("warehouse", dbMap.get("warehouse"));
                // connectionProperties.put("insecureMode", env.getProperty("app.datasource.insecuremode"));
                dataSource.setConnectionProperties(connectionProperties);
                mapDataSourceMap.put(dbMap, dataSource);
                return dataSource;
            }else{
                logger.info("===================Using existing DataSource=================");
                return dataSource;
            }
        }

    }


    public Map getDataSourceMap(RequestToken requestToken) {
        Map<String, String > respMap=new HashMap<>();
        String authorization = "";
        if(requestToken !=null && StringUtils.isNotEmpty(requestToken.getToken())){
            authorization = requestToken.getToken();
        }
        HttpGet request = new HttpGet(env.getProperty("app.easaas.url"));
        request.setHeader(HttpHeaders.AUTHORIZATION, authorization);
        HttpClient client = HttpClient.newBuilder().build();
        HttpResponse response;
        try {
            response = client.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode ==200){
                String result = EntityUtils.toString(response.getEntity());
                JsonElement jelement = new JsonParser().parse(result);
                JsonObject jobject = jelement.getAsJsonObject();
                JsonObject  josonEle = jobject.getAsJsonArray("result").get(0).getAsJsonObject();
                respMap.put("url",josonEle.get("snowflake_url").getAsString().replace("https:","jdbc:snowflake:").replace(".com/",".com"));
                respMap.put("account",creds(josonEle,"account"));
                respMap.put("user",creds(josonEle,"user"));
                respMap.put("warehouse",creds(josonEle,"warehouse"));
                respMap.put("database",creds(josonEle,"database"));
                respMap.put("schema",creds(josonEle,"schema"));
                respMap.put("application", "ElysiumAnalytics");


                respMap.put("queryTimeout", String.valueOf(Integer.parseInt(requestToken.getSearchOptions().get("queryTimeOut"))*60));
                String password= passowrdDecryptionUtil.decryptPassword(requestToken,jobject.getAsJsonObject().get("edata").getAsString());
                respMap.put("password",password);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("================Using session DataSource=======================");
        return respMap;

    }
    private DriverManagerDataSource isDataSourceExist(Map<String,String> dbMap){
        for(Map map:mapDataSourceMap.keySet()){
            if(map.get("url").equals(dbMap.get("url")) &&
                    map.get("password").equals(dbMap.get("password")) &&
                    map.get("account").equals(dbMap.get("account")) &&
                    map.get("database").equals(dbMap.get("database")) &&
                    map.get("schema").equals(dbMap.get("schema")) &&
                    map.get("warehouse").equals(dbMap.get("warehouse")) &&
                    map.get("user").equals(dbMap.get("user")) &&
                    map.get("queryTimeout").equals(dbMap.get("queryTimeout"))){
                return   mapDataSourceMap.get(map);

            }

        }
        return null;
    }


    public String creds(JsonObject josonEle,String key){
        if(josonEle.has(key)){
            return josonEle.get(key).getAsString();
        }
        else{
            if("database".contains(key)){
                return env.getProperty("app.datasource.db");
            }
            else{
                return env.getProperty("app.datasource."+key+"");
            }
        }
    }

}

*/
