package com.security.Snowflake;

import java.sql.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import com.security.User_Api_Services.JwtAuthenticationFilter;
import com.security.User_Api_Services.UserService;
import com.security.exception.DaoException;
import com.security.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("snowflakeQueryService")
public class SnowflakeQueryServiceImpl implements SnowflakeQueryService {
    @Autowired
    UserService userService;
   /* @Autowired
    SnowflakeQueryDao snowflakeQueryDao;*/
    Map<String,Long> userSessionMap=new ConcurrentHashMap<>();
    @Override
    public Map<String, String> getColumnMetaData(String viewName) throws DaoException {
        SnowflakeQueryDao snowflakeQueryDao = new SnowflakeQueryDaoImpl();
        return snowflakeQueryDao.getColumnMetaData(viewName);
    }
    @Override
    public List<List<Object>> executeQueries(List<String> queryList,List<String> queryIds) throws ServiceException {
        try {
            Connection connection = userService.getSnowflakeCredentils(JwtAuthenticationFilter.getRefreshToken()).getConnection();

            if(userSessionMap.get(JwtAuthenticationFilter.getRefreshToken()) !=null) {
                try {
                    PreparedStatement cancelAll;
                    cancelAll = connection.prepareStatement(
                            "call system$cancel_all_queries(?)");

                    // bind the session identifier as first argument
                    cancelAll.setLong(1,userSessionMap.get(JwtAuthenticationFilter.getRefreshToken()));
                    cancelAll.executeQuery();


                    Statement getSessionIdStmt = connection.createStatement();
                    ResultSet resultSet = getSessionIdStmt.executeQuery("SELECT current_session()");
                    if(resultSet.next()){
                        final Long sessionId = resultSet.getLong(1);
                        userSessionMap.put(JwtAuthenticationFilter.getRefreshToken(),sessionId);
                    }

                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
//                    logger.error("Cancel failed with exception {}",ex);
                }
            }else{
                Statement getSessionIdStmt = connection.createStatement();
                ResultSet resultSet = getSessionIdStmt.executeQuery("SELECT current_session()");
                if(resultSet.next()){
                    final Long sessionId = resultSet.getLong(1);
                    userSessionMap.put(JwtAuthenticationFilter.getRefreshToken(),sessionId);
                }
            }

            List<List<Object>> results =new ArrayList<>();
            List<CompletableFuture<List<Object>>> resultList = new ArrayList<>();
            for(String eachQuery : queryList){
               // CompletableFuture<List<Object>> res = snowflakeQueryDao.getResultsOne(requestToken,eachQuery,connection,queryIds);
               // resultList.add(res);
            }


            CompletableFuture.allOf(resultList.toArray(new CompletableFuture[resultList.size()])).join();
            for(CompletableFuture<List<Object>> eachres : resultList){
                results.add(eachres.get());
            }

            userSessionMap.remove(JwtAuthenticationFilter.getRefreshToken());
            return results;
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
    public void executeAsyncQueries(List<String> queryList) throws ServiceException{
        UserService service = new UserService();
        SnowflakeQueryDao queryDao = new SnowflakeQueryDaoImpl();
        try {
            Connection connection = service.getSnowflakeCredentils(JwtAuthenticationFilter.getRefreshToken()).getConnection();
            List<String> queryIds =new ArrayList<>();
            List<CompletableFuture<List<Object>>> resultList = new ArrayList<>();
            for(String eachQuery : queryList)
                queryDao.getResultsSnowflake(eachQuery, connection, queryIds);
        }
        catch (Exception e) { throw new ServiceException(e); }
    }
}
