package com.security.Snowflake;

import java.util.List;
import java.util.Map;

import com.security.exception.DaoException;
import com.security.exception.ServiceException;
public interface SnowflakeQueryService {
    public List<List<Object>> executeQueries(List<String> queryList,List<String> queryIds) throws ServiceException;
    public Map<String, String> getColumnMetaData(String viewName) throws DaoException;
    public void executeAsyncQueries(List<String> queryList) throws ServiceException;

}
