package com.security.Snowflake;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;



public class PassowrdDecryptionUtil {
    public static final Logger logger = LoggerFactory.getLogger(PassowrdDecryptionUtil.class);
    private static final String key = "a93c9a031ea216f3251c223d18f7cc57";
    public static String decryptPassword(String encryptedPassword) {
        // Decode the message and extract the IV and encrypted data
        JSONObject json = new JSONObject(new String(Base64.decodeBase64(encryptedPassword)));
        byte[] iv = Base64.decodeBase64(json.getString("iv"));
        byte[] encryptedData = Base64.decodeBase64(json.getString("data"));

        //Decode the key and set up the cipher
        byte[] decodedKey = hexStringToByteArray(key);
        SecretKeySpec secretKeySpec = new SecretKeySpec(decodedKey, "AES");
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        }
        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(iv));
        }
        catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new RuntimeException(e);
        }
        // Decrypt the data
        byte[] decryptedData = new byte[0];
        try {
            decryptedData = cipher.doFinal(encryptedData);
            return new String(decryptedData, "UTF-8");
        }
        catch (IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i),16)<<4)+ Character.digit(s.charAt(i+1),16));
        }
        return data;
    }
}