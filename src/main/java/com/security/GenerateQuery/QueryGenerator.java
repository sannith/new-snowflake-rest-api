package com.security.GenerateQuery;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.security.hazel_cast.CacheData;
import com.security.demo.CommonController;
import org.apache.tomcat.util.json.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@RestController
@RequestMapping("/api/v1/search")
public class QueryGenerator {
    @Autowired
    CommonController dc;
    @Autowired
    CacheData cacheData;
    public static List <Integer> conditionList = new CopyOnWriteArrayList<>();
    static int n1 = 1;
    ConcurrentHashMap<Integer,Map<Integer,JsonElement>> mainMap = new ConcurrentHashMap<>();
    static String queryOrder = "";
    static String finalQueryOrder = "";

/*    public static Map<String,Map<String,String>> findMap = new HashMap<>();
   public static Map<String, String> getResults(String payLoad){
       final ObjectMapper mapper = new ObjectMapper();
       final MapType type = mapper.getTypeFactory().constructMapType(Map.class, String.class, Object.class);
       final Map<String, Object> map;
       try {
           map = mapper.readValue(payLoad, type);
       }
       catch (IOException e) {
           throw new RuntimeException(e);
       }
       Map<String, String> keyMap = JsonMapFlattener.flattenToStringMap(map);
            //matchFound(keyMap,payLoad);
      // System.out.println("hello1");
       for (String str:keyMap.keySet()) {
           //System.out.println("hello2");
           //System.out.println(str+"="+keyMap.get(str));
       }
       return keyMap;
   }
   private static Map<String, String> matchFound(Map<String,String> keyMap,String s){
       Map<String,String> findMap = new HashMap<>();
       int i = 0;
       //System.out.println(s);
       for (String str:keyMap.keySet()) {
           String search = keyMap.get(str);
          // System.out.println(search);
           if(search.matches(s+".*")){
               findMap.put(""+i,search);
              // System.out.println("Hello");
               i++;
           }
       }
       for (int k=0;k<findMap.size();k++) {
          // System.out.println(findMap.get(""+k));
       }
       return findMap;
   }*/

    @GetMapping("/query")
    public ResponseEntity<String> sayHello(@RequestBody String content) throws ParseException {

        System.out.println("And the data is : "+cacheData.getCachedata("map1","userdata"));
       JsonParser parser = new JsonParser();
       JsonElement json = parser.parse(content);

        JsonElement table = json.getAsJsonObject().get("params").getAsJsonObject().get("index");
        JsonElement query = json.getAsJsonObject().get("params").getAsJsonObject().get("body").getAsJsonObject().get("query");
        Map<Integer,JsonElement> first = new HashMap<>();
        first.put(0,query);
        mainMap.put(0,first);
        iterateQuery(first,0);
        System.out.println("("+finalQueryOrder+")");
        generateQuery(finalQueryOrder,mainMap);
        return ResponseEntity.ok(query.toString());
    }

    private void iterator(){
        for(int mapName : mainMap.keySet())
            if(mainMap.get(mapName)!=null && !(conditionList.contains(mapName)))
                    iterateQuery(mainMap.get(mapName), mapName);
    }
    private void iterateQuery(Map<Integer,JsonElement> main,int n) {
        for (int key : main.keySet()) {
            JsonElement query = main.get(key);
            String mapName = "map";
            if (query.getAsJsonObject().get("bool") != null) {
                String order = "", q1="", q2="", q3="", q4="" ;
                JsonElement filter = query.getAsJsonObject().get("bool").getAsJsonObject().get("filter"),
                            should = query.getAsJsonObject().get("bool").getAsJsonObject().get("should"),
                            must = query.getAsJsonObject().get("bool").getAsJsonObject().get("must"),
                            must_not = query.getAsJsonObject().get("bool").getAsJsonObject().get("must_not");

                if(filter!=null && filter.getAsJsonArray().toString().matches("\\["+".+"+"\\]"))
                    q1 = filter.getAsJsonArray().get(0).toString();

                if(should!=null && should.getAsJsonArray().toString().matches("\\["+".+"+"\\]"))
                    q2 = should.getAsJsonArray().get(0).toString();

                if(must!=null && must.getAsJsonArray().toString().matches("\\["+".+"+"\\]"))
                    q3 = should.getAsJsonArray().get(0).toString();

                if(must_not!=null && must_not.isJsonArray())
                    if (must_not != null && must_not.getAsJsonArray().toString().matches("\\[" + ".+" + "\\]"))
                        q4 = must_not.getAsJsonArray().get(0).toString();

                else if(must_not!=null && must_not.isJsonObject())
                    if(must_not != null && must_not.getAsJsonObject().toString().matches("\\{" + ".+" + "\\}"))
                        q4 = must_not.getAsJsonObject().toString();

                if(n1==1){
                    Map<Integer, JsonElement> map;
                    if (filter!=null && filter.getAsJsonArray().toString().matches("\\[.+\\]")) { iterateFilter(filter,n,mapName);finalQueryOrder=finalQueryOrder+queryOrder;queryOrder="";}
                    if (should!=null && should.getAsJsonArray().toString().matches("\\[.+\\]"))  { iterateShould(should,n,mapName);finalQueryOrder=finalQueryOrder+queryOrder;queryOrder="";}
                    if (must!=null && must.getAsJsonArray().toString().matches("\\[.+\\]"))       { iterateMust(must,n,mapName);finalQueryOrder=finalQueryOrder+queryOrder;queryOrder="";}
                    if (must_not!=null && must_not.getAsJsonArray().toString().matches("\\[.+\\]")) { iterateMust_not(must_not,n,mapName);finalQueryOrder=finalQueryOrder+queryOrder;queryOrder="";}
                    return;
                }
                if ( (q1!="" && (q1.startsWith("{\"bool\""))||q1.startsWith("{\"multi_match\"")||q1.startsWith("{\"query_string\"")) || (q2!="" && (q2.startsWith("{\"bool\"")||q2.startsWith("{\"multi_match\"")||q2.startsWith("{\"query_string\""))) || (q3!="" && (q3.startsWith("{\"bool\"")||q3.startsWith("{\"multi_match\"")||q3.startsWith("{\"query_string\"")) || (q4!="" && (q4.startsWith("{\"bool\""))||q4.startsWith("{\"multi_match\"")||q4.startsWith("{\"query_string\"")||q4.startsWith("{\"match_phrase\"")) || (q4!="" && (q4.startsWith("{\"match_phrase\"")||q4.startsWith("{\"query_string\""))))) {

                        if (query.getAsJsonObject().get("bool").getAsJsonObject().get("filter") != null)
                            iterateFilter(filter,n,mapName);
                        else if (query.getAsJsonObject().get("bool").getAsJsonObject().get("should") != null)
                            iterateShould(should,n,mapName);
                        else if (query.getAsJsonObject().get("bool").getAsJsonObject().get("must") != null)
                            iterateMust(must,n,mapName);
                        else if (query.getAsJsonObject().get("bool").getAsJsonObject().get("must_not") != null)
                            iterateMust_not(must_not,n,mapName);
                        }
                    }
                }
            }

    private void iterateFilter(JsonElement filter,int n,String mapName){
        conditionList.add(n);
        String order="";
        for (int i = 0; i < filter.getAsJsonArray().size(); i++) {
            Map<Integer, JsonElement> map = new HashMap<>();
            map.put(i, filter.getAsJsonArray().get(i));
            if (filter.getAsJsonArray().size() == 1 || i==0)
                order = "("+order + mapName + n1+")";
            else
                order = order + " and (" + mapName + n1+")";
            if (filter != null) {
                String q = filter.getAsJsonArray().get(i).toString();
                if (q.startsWith("{\"bool\"") || q.startsWith("{\"range\"") || q.startsWith("{\"match_phrase\"")|| q.startsWith("{\"multi_match\"")||q.startsWith("{\"query_string\"")||q.startsWith("{\"match_all\""))
                    mainMap.put(n1, map);
            }
            n1++;
        }
        if (queryOrder != "" && queryOrder.matches(".*map" + n + ".*"))
            queryOrder = queryOrder.replace("map" + n, order);
        else if (queryOrder == "")
            queryOrder = queryOrder+order;
        iterator();
    }
    private void iterateShould(JsonElement should,int n,String mapName){
        conditionList.add(n);
        String order="";
        for (int i = 0; i < should.getAsJsonArray().size(); i++) {
            Map<Integer, JsonElement> map = new HashMap<>();
            map.put(i, should.getAsJsonArray().get(i));
            if (should.getAsJsonArray().size() == 1 || i == 0)
                order = order + mapName + n1;
            else
                order = "("+order + ") or (" + mapName + n1+")";
            if (should != null) {
                String q = should.getAsJsonArray().get(i).toString();
                if (q.startsWith("{\"bool\"") || q.startsWith("{\"range\"")|| q.startsWith("{\"multi_match\"")||q.startsWith("{\"query_string\"")||q.startsWith("{\"match_all\"")|| q.startsWith("{\"match_phrase\""))
                    mainMap.put(n1, map);
            }
            n1++;
        }
        if (queryOrder != "" && queryOrder.matches(".*map" + n + ".*"))
            queryOrder = queryOrder.replace("map" + n, order);
        else if (queryOrder == "")
            queryOrder = queryOrder+order;
        iterator();
    }
    private void iterateMust(JsonElement must,int n,String mapName){
        conditionList.add(n);
        String order="";
        for (int i = 0; i < must.getAsJsonArray().size(); i++) {
            Map<Integer, JsonElement> map = new HashMap<>();
            map.put(i, must.getAsJsonArray().get(i));
            if (must.getAsJsonArray().size() == 1 && i == 0)
                order = order + mapName + n1;
            else
                order = "(" + order + ") and (" + mapName + n1 + ")";
            if (must != null) {
                String q = must.getAsJsonArray().get(i).toString();
                if (q.startsWith("{\"bool\"") || q.startsWith("{\"range\"") || q.startsWith("{\"match_phrase\"")|| q.startsWith("{\"multi_match\"")||q.startsWith("{\"query_string\"")||q.startsWith("{\"match_all\""))
                    mainMap.put(n1, map);
            }
            n1++;
        }
        if (queryOrder != "" && queryOrder.matches(".*map" + n + ".*"))
            queryOrder = queryOrder.replace("map" + n, order);
        else if (queryOrder == "")
            queryOrder = queryOrder+order;
        iterator();
    }
    private void iterateMust_not(JsonElement must_not,int n,String mapName) {
        conditionList.add(n);
        String order="";
        if (must_not.isJsonObject()) {
            if (must_not != null) {
                String q = must_not.getAsJsonObject().toString();
                if (q.startsWith("{\"bool\"") || q.startsWith("{\"range\"") || q.startsWith("{\"match_phrase\"")|| q.startsWith("{\"multi_match\"")||q.startsWith("{\"query_string\"")||q.startsWith("{\"match_all\"")) {
                    Map<Integer, JsonElement> map = new HashMap<>();
                    map.put(0, must_not);
                    order = "not(" + order + mapName + n1 + ")";
                    mainMap.put(n1, map);
                }
            }
            n1++;
            if (queryOrder != "" && queryOrder.matches(".*map" + n + ".*"))
                queryOrder = queryOrder.replace("map" + n, order);
            else if (queryOrder == "")
                queryOrder = queryOrder+order;
            iterator();
        }
        if (must_not.isJsonArray()) {
            for (int i = 0; i < must_not.getAsJsonArray().size(); i++) {
                Map<Integer, JsonElement> map = new HashMap<>();
                map.put(i, must_not.getAsJsonArray().get(i));
                if (must_not.getAsJsonArray().size() == 1 || i == 0)
                    order = " and not(" + order + mapName + n1 + ")";
                else
                    order = "(" + order + ") not(" + mapName + n1 + ")";
                if (must_not != null) {
                    String q = must_not.getAsJsonArray().get(i).toString();
                    if (q.startsWith("{\"bool\"") || q.startsWith("{\"range\"") || q.startsWith("{\"match_phrase\"")|| q.startsWith("{\"multi_match\"")||q.startsWith("{\"query_string\"")||q.startsWith("{\"match_all\""))
                        mainMap.put(n1, map);
                }
                n1++;
            }
            if (queryOrder != "" && queryOrder.matches(".*map" + n + ".*"))
                queryOrder = queryOrder.replace("map" + n, order);
            else if(queryOrder=="")
                queryOrder = queryOrder+order;
            iterator();
        }
    }
    private static void generateQuery(String finalQueryOrder,ConcurrentHashMap<Integer,Map<Integer,JsonElement>> mainMap){
        String m = "map";
        for(int i = 0; i<mainMap.size(); i++){
            if(finalQueryOrder.matches(".*\\(map"+i+"\\).*")){
                String modify=getData(i,mainMap.get(i));
                finalQueryOrder=finalQueryOrder.replaceAll(m+i,modify);
               // System.out.println("the final order is : "+finalQueryOrder);
            }
        }
        System.out.println("This is the final query where condition : ("+finalQueryOrder+")");
    }
    private static String getData(int mId,Map<Integer,JsonElement> mp){
        if(mId==10)
            System.out.println("Here is the bug..."+mp);
        String data="000";
        for(int id:mp.keySet()){
            if(mp.get(id).getAsJsonObject().get("bool")!=null){
                if(mp.get(id).getAsJsonObject().get("bool").getAsJsonObject().has("should")){
                    System.out.println("--------->"+mp.get(id).getAsJsonObject().get("bool").getAsJsonObject().get("should").getAsJsonArray().get(0));
                    mp.replace(id,mp.get(id).getAsJsonObject().get("bool").getAsJsonObject().get("should").getAsJsonArray().get(0));
                }
            }
            System.out.println("This is the map"+mId+"------>"+mp.get(id));
            data = caseStatements(mp.get(id).getAsJsonObject().keySet().iterator().next().toString(),mp.get(id));
        }
        return data;
    }
    private static String caseStatements(String caseKey,JsonElement je){
        String data="0";
        switch (caseKey){
            case "query_string":
                String columnName="";
                if(je.getAsJsonObject().get("query_string").getAsJsonObject().has("fields")) {
                    columnName = je.getAsJsonObject().get("query_string").getAsJsonObject().get("fields").getAsJsonArray().iterator().next().toString().replaceAll("\\*","%");
                    System.out.println(columnName);
                }
                String columnValue = je.getAsJsonObject().get("query_string").getAsJsonObject().get("query").toString();
               //columnValue="*satish*";
                if(columnValue.contains("*")||columnValue.contains("\\\\")){
                    columnValue=columnValue.replaceAll("\\*","%");
                    columnValue=columnValue.replaceAll("\\\\","");
                }
                System.out.println(columnName+columnValue);
                data = columnName+"="+columnValue;
                break;

            case "multi_match":
                String type = je.getAsJsonObject().get("multi_match").getAsJsonObject().get("type").toString().replaceAll("\"","");
                switch (type){
                    case "phrase" :
                        System.out.println("="+je.getAsJsonObject().get("multi_match").getAsJsonObject().get("query"));
                        data = "="+je.getAsJsonObject().get("multi_match").getAsJsonObject().get("query").toString();
                        break;
                    case "best_fields" :
                        System.out.println(je.getAsJsonObject().get("multi_match").getAsJsonObject().get("query").toString().replaceAll("\"",""));
                        data = je.getAsJsonObject().get("multi_match").getAsJsonObject().get("query").toString().replaceAll("\"","");
                        break;
                }
                break;

            case "match":
                String colName = je.getAsJsonObject().get("match").getAsJsonObject().keySet().iterator().next().toString();
                System.out.println("Column name is ------> "+colName);
                System.out.println("Column value is ------> "+je.getAsJsonObject().get("match").getAsJsonObject().get(colName).toString().replaceAll("\\*","%"));
                data = colName+"="+je.getAsJsonObject().get("match").getAsJsonObject().get(colName).toString().replaceAll("\\*","%");
                break;

            case "match_phrase":
                String colname = je.getAsJsonObject().get("match_phrase").getAsJsonObject().keySet().iterator().next().toString();
                System.out.println("Column name is ------> "+colname);
                System.out.println("Column value is ------> ="+je.getAsJsonObject().get("match_phrase").getAsJsonObject().get(colname));
                data = colname+"="+je.getAsJsonObject().get("match_phrase").getAsJsonObject().get(colname).toString();
                break;
            case "exists":
                String col=je.getAsJsonObject().get("exists").getAsJsonObject().get("field").toString();
                System.out.println("Column name is ------> "+col);
                data=col;
                break;
            case "match_all":
                String dataa=je.getAsJsonObject().get("match_all").toString();
                if(dataa==null||dataa=="") System.out.println("Select All data from the table...");
                data="Select All data from the table...";
                break;

            case "range":
                String gte = je.getAsJsonObject().get("range").getAsJsonObject().get("event_time").getAsJsonObject().get("gte").toString().replaceAll("\"","");
                String lte = je.getAsJsonObject().get("range").getAsJsonObject().get("event_time").getAsJsonObject().get("lte").toString().replaceAll("\"","");
                System.out.println(gte+" and "+lte);
                data = gte+" and "+lte;
                break;
        }
        return data;
    }
}

