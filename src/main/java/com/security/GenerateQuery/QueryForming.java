    package com.security.GenerateQuery;

    import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.List;
    import java.util.regex.Pattern;
    public class QueryForming {
        public static String ppl_to_sql_converter(String ppl_command) {
            ppl_command = ppl_command.toLowerCase();
            String[] parsed_list = ppl_command.split("\\|");
            for (int i = 0; i < parsed_list.length; i++) {
                parsed_list[i] = parsed_list[i].trim();
            }
            String select_stmt = "SELECT * ";
            String from_stmt = " FROM ";
            String where_stmt = "";
            String group_by = "";
            String order_by = "";
            String limit = " LIMIT 50";
            List<String> select_list = new ArrayList<String>();
            for (String parsed_item : parsed_list) {
                if (Pattern.matches(".*source\\s*.*", parsed_item)) {
                    String[] source_stmt = parsed_item.split("=");
                    String table_name = source_stmt[1].split(" ")[0];
                    from_stmt += table_name;
                } else if (Pattern.matches(".*fields\\s*.*", parsed_item)) {
                    String[] field = parsed_item.split(",");
                    field[0]=field[0].split(" ")[1];
                    String fields = String.join(",", field);
                    select_list.add(fields);
                } else if (Pattern.matches(".*dedup\\s*.*", parsed_item)) {
                    String[] fields = parsed_item.split(" ")[1].split(",");
                    String distinct_fields = "DISTINCT " + String.join(" ", fields);
                    select_list.add(distinct_fields);
                } else if (Pattern.matches(".*eval\\s*.*", parsed_item)) {
                    String[] eval_list = parsed_item.split(",");
                    for (int i = 0; i < eval_list.length; i++) {
                        String[] eval_item = eval_list[i].split("=");
                        eval_list[i] = eval_item[0] + " AS " + eval_item[1];
                    }
                    select_list.addAll(Arrays.asList(eval_list));
                } else if (Pattern.matches(".*where\\s.*", parsed_item)) {
                    where_stmt = " WHERE " + parsed_item.split(" ")[1];
                } else if (Pattern.matches(".*group\\s*by\\s*.*", parsed_item)) {
                    group_by = " GROUP BY " + parsed_item.split(" ", 2);
                } else if (Pattern.matches(".*order\\s*by\\s*.*", parsed_item)) {
                    order_by = " ORDER BY " + parsed_item.split(" ", 2);
                } else if (Pattern.matches(".*limit\\s*.*", parsed_item)) {
                    limit = " LIMIT " + parsed_item.split(" ", 1);
                }
            }
            if (!select_list.isEmpty()) {
                select_stmt = "SELECT " + String.join(", ", select_list);
            }
            String sql_query = select_stmt + from_stmt + where_stmt + group_by + order_by + limit;
            return sql_query;
        }
    }