package com.security.demo;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import java.util.Map;
@Service
public class CommonController {
  public void getAuthorization()  {
    ServletRequestAttributes attr = (ServletRequestAttributes)
            RequestContextHolder.currentRequestAttributes();
    HttpServletRequest request = attr.getRequest();
    String token = request.getHeader("authorization");
    String payLoad = token.split("\\.")[1];
    byte[] byteArray = Base64.decodeBase64(payLoad.getBytes());
    String decodedString = new String(byteArray);
    ObjectMapper mapper = new ObjectMapper();
    Map<String,Object> tokenMap;
    try { tokenMap = mapper.readValue(decodedString,Map.class); }
    catch (JsonMappingException e)   {  throw new RuntimeException(e); }
    catch (JsonProcessingException e){  throw new RuntimeException(e); }
  }

}
