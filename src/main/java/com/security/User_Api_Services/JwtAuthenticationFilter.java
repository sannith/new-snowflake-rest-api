package com.security.User_Api_Services;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
  @Autowired
  JwtService jwtService;
  @Autowired
  UserService userService;
  private static String refreshToken;
  private static String user_id;
  private static String session_id;
  public static String getRefreshToken() {return refreshToken;}
  public void setRefreshToken(String refreshToken) {this.refreshToken = refreshToken;}
  public static String getUser_id() {return user_id;}
  public void setUser_id(String user_id) {this.user_id = user_id;}
  public static String getSession_id() {return session_id;}
  public void setSession_id(String session_id) {this.session_id = session_id;}
  @Override
  protected void doFilterInternal(@NonNull HttpServletRequest request,HttpServletResponse response,FilterChain filterChain) throws ServletException,IOException {

    final String authHeader = request.getHeader("Authorization");
    if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
      filterChain.doFilter(request, response);
      return;
    }
    setRefreshToken(authHeader.substring(7));
    if (SecurityContextHolder.getContext().getAuthentication() == null) {
      if (jwtService.validateToken(refreshToken)) {
        String token = new String(Base64.getDecoder().decode(refreshToken.split("\\.")[1].getBytes()));
        JsonObject user = new JsonParser().parse(token).getAsJsonObject();
        setUser_id(user.get("user_id").toString().replaceAll("\"",""));
        setSession_id(user.get("jti").toString().replaceAll("\"",""));
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(null, null, null);
        authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authToken);
      }
    }
    filterChain.doFilter(request, response);
  }
}
