package com.security.User_Api_Services;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.security.Snowflake.PassowrdDecryptionUtil;
import com.security.hazel_cast.CacheData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


@Component
public class UserService {
    /*@Autowired
    CacheData cacheData;*/
    String uri = "https://dev-saas-api.elysiumanalytics.ai/api/v2";
    HttpEntity httpEntity;
    RestTemplate restTemplate;
    HttpHeaders headers;
    public String getUser(String refreshToken) {
        String temp=uri;
        uri = temp+"/auth/jwt/refresh/";
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("refresh",refreshToken);
        httpEntity = new HttpEntity<>(body);
        restTemplate = new RestTemplate();
        String result = restTemplate.exchange(uri, HttpMethod.POST,httpEntity,String.class).getBody();
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(result);
        result = json.getAsJsonObject().get("access").toString().replaceAll("\"","");

        uri = temp+"/users/who-am-i";
        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", String.format("Bearer %s", result));
        httpEntity = new HttpEntity<>(headers);
        restTemplate = new RestTemplate();
        result = restTemplate.exchange(uri, HttpMethod.GET,httpEntity,String.class).getBody();
        return result;
    }

    public DataSource getSnowflakeCredentils(String refreshToken){
        DriverManagerDataSource dataSource=new DriverManagerDataSource();
        CacheData cacheData = new CacheData();
        String temp=uri;
        uri = temp+"/auth/jwt/refresh/";
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("refresh",refreshToken);
        httpEntity =  new HttpEntity<>(body);
        restTemplate = new RestTemplate();
        String result = restTemplate.exchange(uri, HttpMethod.POST,httpEntity,String.class).getBody();
        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(result);
        result = json.getAsJsonObject().get("access").toString().replaceAll("\"","");

        uri = temp+"/database/info/active-connection";
        headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", String.format("Bearer %s",result));
        httpEntity = new HttpEntity<>(headers);
        restTemplate = new RestTemplate();
        result = restTemplate.exchange(uri, HttpMethod.GET,httpEntity,String.class).getBody();
        result=result.substring(1,result.length()-1);

        JsonObject jsonObject = JsonParser.parseString(result).getAsJsonObject();
        JsonObject db_info = jsonObject.get("database_info").getAsJsonObject();

        System.out.println("========> "+PassowrdDecryptionUtil.decryptPassword(db_info.get("password").getAsString()));
        Map<String,String> info = new HashMap<>();
        info.put("url",db_info.get("host_url").getAsString());
        info.put("account",db_info.get("account").getAsString());
        info.put("user",db_info.get("user").getAsString());
        info.put("password", PassowrdDecryptionUtil.decryptPassword(db_info.get("password").getAsString()).replaceAll("\"",""));
        info.put("database",db_info.get("database").getAsString());
        info.put("warehouse",db_info.get("warehouse").getAsString());
        info.put("schema",db_info.get("schema").getAsString());
        info.put("role",db_info.get("role").getAsString());
        info.put("application", "ElysiumAnalytics");

        dataSource.setUrl("jdbc:snowflake://"+db_info.getAsJsonObject().get("account").getAsString()+".snowflakecomputing.com");
        dataSource.setUsername(db_info.getAsJsonObject().get("user").getAsString());
        dataSource.setPassword(PassowrdDecryptionUtil.decryptPassword(db_info.get("password").getAsString()).replaceAll("\"",""));

        Properties connectionProperties = new Properties();
        connectionProperties.put("account", db_info.getAsJsonObject().get("account").getAsString());
        connectionProperties.put("db", db_info.getAsJsonObject().get("database").getAsString());
        connectionProperties.put("schema", db_info.getAsJsonObject().get("schema").getAsString());
        connectionProperties.put("application", "ElysiumAnalytics");
        //connectionProperties.put("queryTimeout",  String.valueOf(Integer.parseInt(requestToken.getSearchOptions().get("queryTimeOut"))*60));
        connectionProperties.put("warehouse", db_info.getAsJsonObject().get("warehouse").getAsString());
        dataSource.setConnectionProperties(connectionProperties);

        cacheData.setCacheData("user",jsonObject.get("id").getAsString(),info);
        return dataSource;
    }
}
