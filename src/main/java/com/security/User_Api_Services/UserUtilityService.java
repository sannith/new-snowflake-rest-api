package com.security.User_Api_Services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserUtilityService {
    final static Map<String,Object> userMap = new HashMap<>();
    public Map<String,String> getUserInfo(String user_id, String session_id) {
       if(userMap.containsKey(user_id) && userMap.containsKey(session_id)){
           return (Map<String, String>) userMap.get(session_id);
       }
       else{
           if(userMap.containsKey(user_id)){
               userMap.remove(userMap.get(user_id));
               userMap.remove(user_id);
           }
           userMap.put(session_id,getUserDetails());
           userMap.put(user_id,session_id);
           return (Map<String, String>) userMap.get(session_id);
       }
    }
    public Map<String,String> getUserDetails() {
        final ObjectMapper mapper = new ObjectMapper();
        final MapType type = mapper.getTypeFactory().constructMapType(Map.class, String.class, String.class);
        UserService userService = new UserService();
        String userData = userService.getUser(JwtAuthenticationFilter.getRefreshToken());
        final Map<String,String> map;
        try { map = mapper.readValue(userData, type); }
        catch (IOException e) {throw new RuntimeException(e);}
        return map;
    }

}
