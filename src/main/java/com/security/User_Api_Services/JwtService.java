package com.security.User_Api_Services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
@Service
public class JwtService {
  private static final String SECRET_KEY ="django-insecure-042y@okbjq_8wyro_5%+^t45zs2e*1p+00gf@)s46i=z7gl$5#";
     public String generateToken(String subject) {
         Date now = new Date();
         Date expiryDate = new Date(now.getTime() + 86400000); // Token expires after 24 hours
         Key key = Keys.hmacShaKeyFor(SECRET_KEY.getBytes());
         Map<String, Object> extraClaims = new HashMap<>();
         extraClaims.put("user_id","admin");
         String token = Jwts.builder().setHeaderParam("typ", "JWT").setClaims(extraClaims)
                        .setIssuedAt(now).setExpiration(expiryDate).signWith(key, SignatureAlgorithm.HS256)
                        .compact();
         return token;
     }
    public Claims parseToken(String token) {
        Key key = Keys.hmacShaKeyFor(SECRET_KEY.getBytes());
        Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
        return claims;
    }
    public boolean validateToken(String token) {
        try {
        Key key = Keys.hmacShaKeyFor(SECRET_KEY.getBytes());
        Jwts.parser().setSigningKey(key).parseClaimsJws(token);
        return true;
        }
        catch (Exception ex) {  return false;  }
    }
  }
