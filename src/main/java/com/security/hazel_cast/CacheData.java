package com.security.hazel_cast;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;


@Service("cacheData")
@PropertySource({ "classpath:application.properties" })
public class CacheData {
    private HazelcastInstance client = null;

    public CacheData() {
        if (this.client == null)
            connectToCacheClient();
    }
    public HazelcastInstance connectToCacheClient() {
        ClientConfig clientConfig = new ClientConfig();
        ClientNetworkConfig networkConfig = clientConfig.getNetworkConfig();
        // networkConfig.addAddress("10.10.150.37");
        networkConfig.addAddress("localhost");// , "127.0.0.2");
        // Create a client side HazelcastInstance
        this.client = HazelcastClient.newHazelcastClient(clientConfig);
        String instanceName = client.getName();
        System.out.println(instanceName);
        return this.client;
    }


    public boolean ifKeyExistsInCache(String mapName, String cacheKey) {
        return this.client.getMap(mapName).containsKey(cacheKey);
    }
    public void removecache(String mapName, String cacheKey) {
        this.client.getMap(mapName).remove(cacheKey);
    }

    public Object getCachedata(String mapName, String cacheKey) {
        return this.client.getMap(mapName).get(cacheKey);
    }

    public void setCacheData(String mapName, String cacheKey, Object cacheValue) {
        this.client.getMap(mapName).put(cacheKey, cacheValue);
    }
}
